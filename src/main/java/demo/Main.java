package demo;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;

import java.net.URL;
import java.security.Security;

public class Main {

  public static void main(String[] args) {

    if (args.length < 1) {
      System.out.println("Please provide a token");
      return;
    }

    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

    // The access token to validate, typically submitted with a HTTP header like
    // Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6InMxIn0.eyJzY3A...
    String googleToken = args[0];
    String googleJwk = "https://www.googleapis.com/oauth2/v3/certs";

    try {
      // 1. original token
      System.out.println("Original token\n" + googleToken + "\n");
      System.out.println("Validate token...");

      validateToken(googleToken, googleJwk);

      // 2. modified token
      char lastChar = googleToken.charAt(googleToken.length() - 1);
      char modChar = (char) (lastChar + 1);

      System.out.println("Modify last char " + lastChar + " => " + modChar + "\n");

      String modifiedToken = googleToken.substring(0, googleToken.length() - 1) + modChar;
      System.out.println("Modified token\n" + modifiedToken + "\n");
      System.out.println("Validate token...");

      validateToken(modifiedToken, googleJwk);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void validateToken(String token, String jwkUri) throws Exception {
    // Set up a JWT processor to parse the tokens and then check their signature
    // and validity time window (bounded by the "iat", "nbf" and "exp" claims)
    ConfigurableJWTProcessor jwtProcessor = new DefaultJWTProcessor();

    // The public RSA keys to validate the signatures will be sourced from the
    // OAuth 2.0 server's JWK set, published at a well-known URL. The RemoteJWKSet
    // object caches the retrieved keys to speed up subsequent look-ups and can
    // also gracefully handle key-rollover
    JWKSource keySource = new RemoteJWKSet(new URL(jwkUri));

    // The expected JWS algorithm of the access tokens (agreed out-of-band)
    JWSAlgorithm expectedJWSAlg = JWSAlgorithm.RS256;

    // Configure the JWT processor with a key selector to feed matching public
    // RSA keys sourced from the JWK set URL
    JWSKeySelector keySelector = new JWSVerificationKeySelector(expectedJWSAlg, keySource);
    jwtProcessor.setJWSKeySelector(keySelector);

    // Process the token
    SecurityContext ctx = null; // optional context parameter, not required here
    JWTClaimsSet claimsSet = jwtProcessor.process(token, ctx);

    // Print out the token claims set
    System.out.println(claimsSet.toJSONObject());

  }

  private static String hex(byte[] bytes) {
    StringBuffer hexString = new StringBuffer();
    for (int i = 0; i < bytes.length; i++) {
      hexString.append(Integer.toHexString(0xFF & bytes[i]));
    }
    return hexString.toString();
  }
}
