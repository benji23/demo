# Testing JWT validation with nimbus-jose-jwt #

### How to run ###

Build the project
```
#!bash
mvn clean package
```
Run the jar-with-dependencies with your OIDC token as parameter, e.g.
```
#!bash
export TOKEN=eyJhbG.......3bsGmYg
java -jar target/demo-0.0.1-SNAPSHOT-jar-with-dependencies.jar $TOKEN

```
